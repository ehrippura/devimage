//
//  main.m
//  devImage_cmd
//
//  Created by Tzu-Yi Lin on 13/8/10.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DIImageProcessor.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool {
        NSString *desktopPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Desktop"];
        DIImage *image = [[DIImage alloc] initWithPath:[desktopPath stringByAppendingPathComponent:@"test.png"]];
        DIImage *resize = [DIImageProcessor halfSizeImage:image];

        NSString *targetPath = [desktopPath stringByAppendingPathComponent:@"hello.jpg"];
        [resize writeToFile:targetPath withType:DIImageOutputFormatJPEG];
    }

    return 0;
}

