//
//  DIPreferenceController.m
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/9.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import "DIPreferenceController.h"

NSString *DIPreferenceOutputLocationKey = @"DIPreferenceOutputLocationKey";
NSString *DIPreferenceOutputLocationPathKey = @"DIPreferenceOutputLocationPathKey";
NSString *DIPreferenceOutputFormatKey = @"DIPreferenceOutputFormatKey";
NSString *DIPreferenceShouldKeepsOriginalKey = @"DIPreferenceShouldKeepsOriginalKey";
NSString *DIPreferenceJPEGQualityKey = @"DIPreferenceJPEGQualityKey";

@implementation DIPreferenceController

- (void)awakeFromNib
{
    // set interface
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    [_pathField setStringValue:[prefs stringForKey:DIPreferenceOutputLocationPathKey]];
    [_targetMatrix selectCellAtRow:0 column:[prefs integerForKey:DIPreferenceOutputLocationKey]];
    [self outputTargetChange:nil];
    [_formatMatrix selectCellAtRow:[prefs integerForKey:DIPreferenceOutputFormatKey] column:0];
    [_keepsOriginalButton setState:[prefs boolForKey:DIPreferenceShouldKeepsOriginalKey] ? NSOnState : NSOffState];
    [_jpegQualitySlider setFloatValue:[prefs floatForKey:DIPreferenceJPEGQualityKey]];
    [_qualityLabel setStringValue:[NSString stringWithFormat:@"JPEG Quality: %.1f", [_jpegQualitySlider floatValue]]];
}

- (IBAction)outputTargetChange:(id)sender
{
    BOOL enable = [_targetMatrix selectedTag] == DIPreferenceOutputLocationUserDefine;

    [_browseButton setEnabled:enable];
    [_pathField setEnabled:enable];

    [[NSUserDefaults standardUserDefaults] setInteger:[_targetMatrix selectedTag] forKey:DIPreferenceOutputLocationKey];
}

- (IBAction)outputFormatChange:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setInteger:[_formatMatrix selectedTag] forKey:DIPreferenceOutputFormatKey];
}

- (IBAction)keepsOriginalChange:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:[_keepsOriginalButton state] == NSOnState
                                            forKey:DIPreferenceShouldKeepsOriginalKey];
}

- (IBAction)jpegQualityChange:(id)sender
{
    [_qualityLabel setStringValue:[NSString stringWithFormat:@"JPEG Quality: %.1f", [_jpegQualitySlider floatValue]]];
    [[NSUserDefaults standardUserDefaults] setFloat:[_jpegQualitySlider floatValue] forKey:DIPreferenceJPEGQualityKey];
}

- (IBAction)selectPathButtonDown:(id)sender
{
    NSLog(@"Show Load Panel");
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];

    [openPanel setAllowsMultipleSelection:NO];
    [openPanel setCanChooseFiles:NO];
    [openPanel setCanChooseDirectories:YES];
    [openPanel setCanCreateDirectories:YES];

    [openPanel beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result) {
        if (result == NSOKButton) {
            NSString *path = [[openPanel URL] path];
            [_pathField setStringValue:path];
            [[NSUserDefaults standardUserDefaults] setObject:path forKey:DIPreferenceOutputLocationPathKey];
        }
    }];
}

@end
