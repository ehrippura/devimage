//
//  DIPreferenceController.h
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/9.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

/* setting keys */
extern NSString *DIPreferenceOutputLocationKey;
extern NSString *DIPreferenceOutputLocationPathKey;
extern NSString *DIPreferenceOutputFormatKey;
extern NSString *DIPreferenceShouldKeepsOriginalKey;
extern NSString *DIPreferenceJPEGQualityKey;

typedef enum : NSInteger {
    DIPreferenceOutputFormatSameAsSource    = 0,
    DIPreferenceOutputFormatJPEG            = 1,
    DIPreferenceOutputFormatPNG             = 2
} DIPreferenceOutputFormat;

typedef enum : NSInteger {
    DIPreferenceOutputLocationSameAsSource  = 0,
    DIPreferenceOutputLocationUserDefine    = 1
} DIPreferenceOutputLocation;

@interface DIPreferenceController : NSWindowController <NSTextFieldDelegate> {

    /* target buttons */
    IBOutlet NSMatrix *_targetMatrix;
    IBOutlet NSButton *_browseButton;
    IBOutlet NSTextField *_pathField;

    /* format buttons */
    IBOutlet NSMatrix *_formatMatrix;
    IBOutlet NSButton *_keepsOriginalButton;
    IBOutlet NSSlider *_jpegQualitySlider;
    IBOutlet NSTextField *_qualityLabel;
}

- (IBAction)outputTargetChange:(id)sender;
- (IBAction)outputFormatChange:(id)sender;
- (IBAction)keepsOriginalChange:(id)sender;
- (IBAction)jpegQualityChange:(id)sender;

/* path setting */
- (IBAction)selectPathButtonDown:(id)sender;

@end
