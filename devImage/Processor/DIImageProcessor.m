//
//  DIImageProcessor.m
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/10.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import "DIImageProcessor.h"
#import "DIImage.h"

@interface DIImage (PrivateMethods)
- (void)__setTargetpath:(NSString *)path;
@end

@implementation DIImageProcessor

+ (DIImage *)halfSizeImage:(DIImage *)originalImage
{
    NSImage *sourceImage = [originalImage copy];
    [sourceImage setScalesWhenResized:YES];

    // Report an error if the source isn't a valid image
    if (![sourceImage isValid]) {
        NSLog(@"Invalid Image");
    } else {
        NSImageRep *oriRep = [[originalImage representations] objectAtIndex:0];

        NSSize newSize = (NSSize) { [oriRep size].width / 2, [oriRep size].height / 2 };
        NSRect targetFrame = NSMakeRect(0, 0, newSize.width, newSize.height);
        __autoreleasing DIImage *smallImage = [[DIImage alloc] initWithSize:newSize];

        NSImageRep *targetRep = [originalImage bestRepresentationForRect:targetFrame context:nil hints:nil];

        [smallImage lockFocus];
        [targetRep drawInRect:targetFrame];
        NSBitmapImageRep *bitmap = [[NSBitmapImageRep alloc] initWithFocusedViewRect:targetFrame];
        [smallImage addRepresentation:bitmap];
        [smallImage unlockFocus];

        [smallImage __setTargetpath:originalImage.path];
        
        return smallImage;
    }
    
    return nil;
}

@end
