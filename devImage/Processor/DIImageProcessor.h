//
//  DIImageProcessor.h
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/10.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DIImage.h"

@interface DIImageProcessor : NSObject

+ (DIImage *)halfSizeImage:(DIImage *)originalImage;

@end
