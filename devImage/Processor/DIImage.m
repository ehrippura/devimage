//
//  DIImage.m
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/10.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import "DIImage.h"

////////////////////////////////////////////////////////////////////////////////

@implementation NSImage (DIImage)

- (CGImageRef)copyToCGImage
{
    NSData * imageData = [self TIFFRepresentation];
    if(!imageData) return NULL;

    NSDictionary *options = @{(id)kCGImageSourceShouldAllowFloat: (id)kCFBooleanTrue};
    
    CGImageSourceRef imageSource = CGImageSourceCreateWithData((__bridge CFDataRef)imageData, (__bridge CFDictionaryRef)options);
    CGImageRef imageRef = CGImageSourceCreateImageAtIndex(imageSource, 0, (__bridge CFDictionaryRef)options);
    return imageRef;
}

@end

////////////////////////////////////////////////////////////////////////////////
@implementation DIImage

@synthesize path = _path;

- (id)initWithPath:(NSString *)path
{
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:path]) {
        NSData *data = [NSData dataWithContentsOfFile:path];

        if (data) {
            self = [super initWithData:data];

            if (self)
                _path = path;

            return self;
        }
    }

    return nil;
}

- (void)__setTargetpath:(NSString *)path
{
    if (![path isEqualToString:_path])
        _path = path;
}

- (void)writeToFile:(NSString *)target withType:(DIImageOutputFormat)format
{
    CGImageRef cgImage = [self copyToCGImage];

    if (cgImage) {
        float compression = 1.0; // Lossless compression if available.
        int orientation = 1; // Origin is at bottom, left.
        CFStringRef myKeys[3];
        CFTypeRef   myValues[3];

        if (format == DIImageOutputFormatJPEG) {
            id del = (id <DIImageDelegate>)[self delegate];
            compression = [del respondsToSelector:@selector(jpegQuality)] ? [del jpegQuality] : 0.8;
        }

        CFDictionaryRef myOptions = NULL;
        myKeys[0] = kCGImagePropertyOrientation;
        myValues[0] = CFNumberCreate(NULL, kCFNumberIntType, &orientation);
        myKeys[1] = kCGImagePropertyHasAlpha;
        myValues[1] = kCFBooleanTrue;
        myKeys[2] = kCGImageDestinationLossyCompressionQuality;
        myValues[2] = CFNumberCreate(NULL, kCFNumberFloatType, &compression);
        myOptions = CFDictionaryCreate( NULL, (const void **)myKeys, (const void **)myValues, 3,
                                       &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);

        NSURL *targetURL = [NSURL fileURLWithPath:target];
        CGImageDestinationRef idst = CGImageDestinationCreateWithURL((__bridge CFURLRef)targetURL,
                                                                     format == DIImageOutputFormatPNG ? kUTTypePNG : kUTTypeJPEG,
                                                                     1, nil);
        CGImageDestinationAddImage(idst, cgImage, myOptions);
        CGImageDestinationFinalize(idst);
        CFRelease(idst);

        CFRelease(myValues[0]);
        CFRelease(myValues[2]);
        CFRelease(myOptions);

        CFRelease(cgImage);
    }
    
}

@end

