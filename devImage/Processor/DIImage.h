//
//  DIImage.h
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/10.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSImage (DIImage)

- (CGImageRef)copyToCGImage;

@end

////////////////////////////////////////////////////////////////////////////////

typedef enum DIImageOutputFormat : NSUInteger {
    DIImageOutputFormatJPEG     = NSJPEGFileType,
    DIImageOutputFormatPNG      = NSPNGFileType,
    DIImageOutputFormatNotDefine = NSUIntegerMax,
} DIImageOutputFormat;

@interface DIImage : NSImage {

@private
    NSString *_path;
}

@property (nonatomic, readonly) NSString *path;

- (id)initWithPath:(NSString *)path;
- (void)writeToFile:(NSString *)target withType:(DIImageOutputFormat)format;

@end


@protocol DIImageDelegate <NSImageDelegate>

- (CGFloat)jpegQuality;

@end
