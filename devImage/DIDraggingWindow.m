//
//  DIDraggingWindow.m
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/10.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import "DIDraggingWindow.h"
#import "DIAppController.h"


@implementation DIDraggingWindow

- (void)awakeFromNib
{
    // register drop operation
    [self registerForDraggedTypes:@[NSFilenamesPboardType]];
}

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender
{
    [self.appController showArrowImage:YES animated:NO];

    NSDragOperation mask = [sender draggingSourceOperationMask];
    NSPasteboard *pastboard = [sender draggingPasteboard];

    if ([[pastboard types] containsObject:NSFilenamesPboardType]) {
        if (mask & NSDragOperationLink)
            return NSDragOperationLink;
    }

    return NSDragOperationNone;
}

- (BOOL)performDragOperation:(id<NSDraggingInfo>)sender
{
    NSPasteboard *pboard = [sender draggingPasteboard];
    NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
    NSMutableArray *m_files = [files mutableCopy];

    for (NSString *filename in files) {
        CFStringRef cf_file = (__bridge CFStringRef)[filename pathExtension];
        CFStringRef ut = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, cf_file, NULL);

        if (!UTTypeConformsTo(ut, kUTTypeImage)) {
            [m_files removeObject:filename];
        }

        if (ut != NULL) CFRelease(ut);
    }

    [self.appController showArrowImage:NO animated:NO];
    [self.appController setCountNumber:[m_files count]];

    if ([m_files count] == 0)
        return NO;

    [self.appController resizeImages:m_files];

    return YES;
}

- (void)draggingExited:(id <NSDraggingInfo>)sender
{
    [self.appController showArrowImage:NO animated:NO];
}

@end
