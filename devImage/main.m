//
//  main.m
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/9.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
