//
//  DIAppDelegate.m
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/9.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import "DIAppDelegate.h"
#import "DIDraggingWindow.h"

@implementation DIAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // register default preference setting
    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"default" ofType:@"plist"];
    NSDictionary *defaults = [NSDictionary dictionaryWithContentsOfFile:filepath];

    if (defaults) {
        [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
    }

    [(DIDraggingWindow *)self.window setAppController:self.appController];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return sender == [NSApplication sharedApplication];
}

@end
