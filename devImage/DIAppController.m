//
//  DIAppController.m
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/9.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import "DIAppController.h"
#import "DIPreferenceController.h"
#import "DIImageProcessor.h"

#import <QuartzCore/CoreAnimation.h>

@interface DIAppController() <DIImageDelegate> {
    DIPreferenceController *_preference;
}

@end

NSString *getNewName(NSString *name)
{
    NSRange range = [name rangeOfString:@"." options:NSBackwardsSearch];
    NSString *target = [name stringByReplacingCharactersInRange:NSMakeRange(range.location, 0) withString:@"_ori"];

    return target;
}

NSString *outputName(NSString *name, DIImageOutputFormat *outputFormat, BOOL _2x)
{
    NSRange range = [name rangeOfString:[name pathExtension] options:NSBackwardsSearch];
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    DIPreferenceOutputFormat format = [pref integerForKey:DIPreferenceOutputFormatKey];

    NSString *formatName = [name pathExtension];
    if (format != DIPreferenceOutputFormatSameAsSource) {
        formatName = (format == DIPreferenceOutputFormatPNG) ? @"png" : @"jpg";
        if (outputFormat)
            *outputFormat = [formatName isEqualToString:@"png"] ? DIImageOutputFormatPNG : DIImageOutputFormatJPEG;
    } else {
        if (outputFormat)
            *outputFormat = DIImageOutputFormatNotDefine;
    }

    NSString *target = [name stringByReplacingCharactersInRange:range
                                                     withString:formatName];

    DIPreferenceOutputLocation location = [pref integerForKey:DIPreferenceOutputLocationKey];
    if (location == DIPreferenceOutputLocationUserDefine) {
        NSString *destination = [pref objectForKey:DIPreferenceOutputLocationPathKey];
        target = [destination stringByAppendingPathComponent:[target lastPathComponent]];
    }
    
    if (_2x) {
        range = [target rangeOfString:@"." options:NSBackwardsSearch];
        target = [target stringByReplacingCharactersInRange:NSMakeRange(range.location, 0) withString:@"@2x"];
    }
    
    return target;
}

BOOL outputPathAccessable()
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];

    if ([pref integerForKey:DIPreferenceOutputLocationKey] == DIPreferenceOutputLocationSameAsSource)
        return YES;

    NSString *path = [pref objectForKey:DIPreferenceOutputLocationPathKey];

    BOOL isDirectory = NO;
    if ([fm fileExistsAtPath:path isDirectory:&isDirectory]) {
        return isDirectory;
    }

    return NO;
}

@implementation DIAppController

- (void)awakeFromNib
{
    // add core animation layer
    CALayer *animateLayer = [CALayer layer];
    [_arrowIcon setLayer:animateLayer];
    [_arrowIcon setWantsLayer:YES];

    [_arrowIcon unregisterDraggedTypes];
    _arrowIcon.layer.opacity = 0;

    [[_countLabel cell] setBackgroundStyle:NSBackgroundStyleRaised];
}

- (IBAction)preferenceShow:(id)sender
{
    if (!_preference)
        _preference = [[DIPreferenceController alloc] initWithWindowNibName:@"DIPreferenceController"];
    
    [_preference showWindow:sender];
}

- (IBAction)goWebsite:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"http://wayne.eternalwind.tw"];
    [[NSWorkspace sharedWorkspace] openURL:url];
}

- (void)showArrowImage:(BOOL)show animated:(BOOL)animated
{
    if (show) {
        [[_arrowIcon layer] setValue:@1 forKey:@"opacity"];
    } else {
        [[_arrowIcon layer] setValue:@0 forKey:@"opacity"];
    }
}

- (void)resizeImages:(NSArray *)imageNames
{
    [_progressIndicator setHidden:NO];
    [_progressIndicator setDoubleValue:0.0];

    if (!outputPathAccessable()) {
        
        NSAlert *alert = [NSAlert alertWithMessageText:@"Destination Disappear"
                                         defaultButton:@"OK"
                                       alternateButton:@"Cancel"
                                           otherButton:nil
                             informativeTextWithFormat:@"Destination %@ dissappeared, save converted files into the same location with source?", [[NSUserDefaults standardUserDefaults] stringForKey:DIPreferenceOutputLocationPathKey]];
        NSInteger result = [alert runModal];
        if (result != NSAlertDefaultReturn) {
            [_progressIndicator setHidden:YES];
            return;
        }
    }
    
    BOOL keepsOriginal = [[NSUserDefaults standardUserDefaults] boolForKey:DIPreferenceShouldKeepsOriginalKey];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSInteger count = [imageNames count];

        for (NSString *filename in imageNames) {
            DIImage *image = [[DIImage alloc] initWithPath:filename];
            if (!image) continue;

            DIImage *resize = [DIImageProcessor halfSizeImage:image];
            resize.delegate = self;

            DIImageOutputFormat format = 0;
            NSString *output = outputName(filename, &format, NO);
            
            // change file name
            NSString *newFilename = getNewName(filename);
            NSString *new2Xname = outputName(filename, NULL, YES);
            
            // rename - copy - remove
            NSFileManager *fm = [NSFileManager defaultManager];
            if (keepsOriginal) {
                [fm moveItemAtPath:filename toPath:newFilename error:nil];
            } else {
                [fm removeItemAtPath:filename error:nil];
            }
            
            [image writeToFile:new2Xname withType:format];
            [resize writeToFile:output
                       withType:format];

            dispatch_async(dispatch_get_main_queue(), ^{
                [_progressIndicator setDoubleValue:((double)[imageNames indexOfObject:filename] + 1.0) / (double)count];

                if ([imageNames indexOfObject:filename] == count - 1) {
                    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
                    [_progressIndicator setHidden:YES];
                }
            });
        }
    });
}

- (void)setCountNumber:(NSUInteger)count
{
    [_countLabel setStringValue:[NSString stringWithFormat:@"%lu images accessible", count]];
}

- (CGFloat)jpegQuality
{
    return [[NSUserDefaults standardUserDefaults] floatForKey:DIPreferenceJPEGQualityKey];
}

@end
