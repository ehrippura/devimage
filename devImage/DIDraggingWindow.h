//
//  DIDraggingWindow.h
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/10.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class DIAppController;

@interface DIDraggingWindow : NSWindow <NSDraggingDestination>

@property (nonatomic, weak) DIAppController *appController;

@end
