//
//  DIAppDelegate.h
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/9.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class DIAppController;

@interface DIAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet DIAppController *appController;

@end
