//
//  DIAppController.h
//  devImage
//
//  Created by Tzu-Yi Lin on 13/8/9.
//  Copyright (c) 2013年 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DIAppController : NSObject {
    IBOutlet NSTextField *_infoText;
    IBOutlet NSImageView *_arrowIcon;
    IBOutlet NSProgressIndicator *_progressIndicator;
    IBOutlet NSTextField *_countLabel;
}

/* UI action */
- (IBAction)preferenceShow:(id)sender;
- (IBAction)goWebsite:(id)sender;

- (void)showArrowImage:(BOOL)show animated:(BOOL)animated;
- (void)resizeImages:(NSArray *)imageNames;

- (void)setCountNumber:(NSUInteger)count;

@end
